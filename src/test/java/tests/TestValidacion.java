/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import com.mycompany.validacionplaca.Validacion;
import junit.framework.TestCase;
import org.junit.jupiter.api.Test;

/**
 *
 * @author ChristGC <alex.gonzalezc@ug.edu.ec>
 */
public class TestValidacion extends TestCase {
    
    private Validacion val;
    
    public void Validar() {
        val=new Validacion();
    }
    
    @Test
    public void testValL1(){
        Validar();
        assertEquals("No aplica", val.valL1('D'));
    }
    
    @Test
    public void testValL2(){
        Validar();
        assertEquals('E', val.valL2("Vehiculo gubernamental"));
    }
    
}
