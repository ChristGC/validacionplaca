/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.validacionplaca;

/**
 * 
 * @author ChristGC <alex.gonzalezc@ug.edu.ec>
 */
public class Validacion {
    
    private char L1,L2,L3;
    
    public String valL1(char l1){
        String prov=null;
        int numL1=l1;
        if(numL1>=65 & numL1<=90) {
            switch(numL1){
                case 65:prov=  "Azuay";break;
                case 66:prov=  "Bolivar"; break;
                case 67:prov="Carchi";break;
                case 69:prov="Esmeraldas";break;
                case 71:prov="Guayas";break;
                case 72:prov="Chimborazo";break;
                case 73:prov="Imbabura";break;
                case 74:prov="Santo Domingo de los Tsachilas";break;
                case 75:prov="Sucumbios";break;
                case 76:prov="Loja";break;
                case 77:prov="Manabi";break;
                case 78:prov="Napo";break;
                case 79:prov="El Oro";break;
                case 80:prov="Pichincha";break;
                case 81:prov="Orellana";break;
                case 82:prov="Los Rios";break;
                case 83:prov="Pastaza";break;
                case 84:prov="Tungurahua";break;
                case 85:prov="Cañar";break;
                case 86:prov="Morona Santiago";break;
                case 88:prov="Cotopaxi";break;
                case 89:prov="Santa Elena";break;
                case 90:prov="Zamora Chinchipe";break;
                default:prov="No aplica";break;                
            }
        }
        return prov;      
    } 
    
    public char valL2(String tipo){
        char l2=0;
        int cod;
        if(tipo.equals("Vehiculo comercial")){
            char[] letras={'A','U','Z'};
            cod = (int)(Math.random()*2);
            l2=letras[cod];
        }else if(tipo.equals("Vehiculo gubernamental")){l2='E';}
        else if(tipo.equals("Vehiculo de uso oficial")){l2='X';}
        else if(tipo.equals("Vehiculo de gobierno autonomo")){l2='M';}
        else if(tipo.equals("Vehiculo particular")){
            cod = (int)(Math.random()*90+65);
            if(cod!=65 && cod!=85 && cod!=90 && cod!=69 && cod!=88 && cod!=77){
                l2= (char)cod;
            }else l2='B';
        }
        return l2;
    }
    
    public static void main(String[] args) {
        
    }
}
